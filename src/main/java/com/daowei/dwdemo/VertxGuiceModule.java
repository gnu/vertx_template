package com.daowei.dwdemo;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import io.vertx.core.Vertx;

public class VertxGuiceModule extends AbstractModule {
  private final Vertx vertx;

  public VertxGuiceModule(Vertx vertx) {
    this.vertx = vertx;
  }

  @Provides
  public Vertx provideVertx() {
    return vertx;
  }

}
