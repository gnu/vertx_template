package com.daowei.dwdemo.utils;

import java.util.Map;

public class SQLModel {
  public final String sql;
  public final Map<String, Object> parameters;

  public SQLModel(String sql, Map<String, Object> parameters) {
    this.sql = sql;
    this.parameters = parameters;
  }

}
