package com.daowei.dwdemo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class SQLUtils {
  private static final Logger LOGGER = LoggerFactory.getLogger(SQLUtils.class);

  public static SQLModel sql(String s, Map<String, Object> parameters) {
    LOGGER.debug("{}, {}", s, parameters == null ? "" : parameters);
    return new SQLModel(s, parameters);
  }




}
