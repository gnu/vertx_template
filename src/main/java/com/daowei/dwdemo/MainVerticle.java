package com.daowei.dwdemo;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class MainVerticle extends AbstractVerticle {

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(new MainVerticle());
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    System.out.println("主Main: " + vertx);
    try {
      final Injector injector = Guice.createInjector(new VertxGuiceModule(vertx));
      vertx.deployVerticle(injector.getInstance(WebServer.class))
          .onComplete(res -> {
            if (res.succeeded()) {
              startPromise.complete();
            } else {
              LOGGER.error("startErr", res.cause());
              startPromise.fail(res.cause());
            }
          });
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public Vertx getVertx() {
    return super.getVertx();
  }
}
