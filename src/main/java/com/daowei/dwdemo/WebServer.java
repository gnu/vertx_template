package com.daowei.dwdemo;

import com.daowei.dwdemo.dao.DatabaseService;
import com.daowei.dwdemo.utils.SQLModel;
import com.daowei.dwdemo.utils.SQLUtils;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.vertx.core.AbstractVerticle;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.templates.SqlTemplate;

import java.util.Collections;

@Singleton
public class WebServer extends AbstractVerticle {

  private final DatabaseService databaseService;

  @Inject
  public WebServer(DatabaseService databaseService) {
    this.databaseService = databaseService;
  }

  @Override
  public void start() throws Exception {
    System.out.println("WebServer: " + vertx);
    vertx.createHttpServer().requestHandler(req -> {
      databaseService.queryBook().onComplete(res -> {
        if (res.succeeded()) {
          req.response().end(res.result().encodePrettily());
        } else {
          req.response().end("error");
        }
      });
    }).listen(9087, res -> {
      if (res.succeeded()) {
        System.out.println("server start ok!");
      } else {
        res.cause().printStackTrace();
      }
    });
  }
}
